from nodePLVR import NodePLVR
from argumentosPLVR import ArgumentosPLVR

import sys


def executarApenasUmNodePLVR():
    argsPLVR = ArgumentosPLVR()

    ''' CONFIGURACOES DO COMUNICADOR SEM FIO '''
    argsPLVR.comunicadorSemFio_start = True
    # argsPLVR.comunicadorSemFio_start = False

    argsPLVR.comunicadorSemFio_nodeID = 0
    # argsPLVR.comunicadorSemFio_nodeID = 1
    # argsPLVR.comunicadorSemFio_nodeID = 2

    argsPLVR.comunicadorSemFio_timeToSleep = 2
    # argsPLVR.comunicadorSemFio_timeToSleep = 3

    argsPLVR.comunicadorSemFio_enderecoIP = '192.168.0.255'     # Testes Bernardo
    # argsPLVR.comunicadorSemFio_enderecoIP = '192.168.1.255'   # Wifi ProjetoPesquisaCaput
    # argsPLVR.comunicadorSemFio_enderecoIP = ''

    argsPLVR.comunicadorSemFio_portaDeEscuta = 5000
    # argsPLVR.comunicadorSemFio_portaDeEscuta = 6000

    argsPLVR.comunicadorSemFio_portaPadraoParaEnviar = 5000
    # argsPLVR.comunicadorSemFio_portaPadraoParaEnviar = 6000
    ''' CONFIGURACOES DO COMUNICADOR SEM FIO '''



    ''' CONFIGURACOES DO RECONHECEDOR DE VEICULO '''
    # argsPLVR.reconhecedorDeVeiculo_start = True
    argsPLVR.reconhecedorDeVeiculo_start = False

    ''' CONFIGURACOES DO RECONHECEDOR DE VEICULO '''



    ''' INICIALIZACAO DO NodePLVR '''
    nodePLVR = NodePLVR(argsPLVR)

    print('main_plvr -> __main__: starting nodePLVR...')
    nodePLVR.start()
    print('main_plvr -> __main__: nodePLVR started')
    print()

    # print(argsPLVR.aaa)




if __name__ == '__main__':

    print('sys.argv:', sys.argv, '\n')

    executarApenasUmNodePLVR()
