from communication.comunicadorPLVR import ComunicadorPLVR
from ocr_veiculos.reconhecedorDeVeiculo import ReconhecedorDeVeiculo
from argumentosPLVR import ArgumentosPLVR


class NodePLVR:

    # metodo construtor
    def __init__(self, argsPLVR=ArgumentosPLVR()):
        self.__argsPLVR = argsPLVR
        self.__comunicadorPLVR = ComunicadorPLVR(argsPLVR)
        self.__reconhecedorDeVeiculo = ReconhecedorDeVeiculo()
        # print('NodePLVR -> __init__() -> argsPLVR.aaa:', argsPLVR.aaa)

    # metodo privado
    def __inicializarComunicadorSemFio(self):
        self.__comunicadorPLVR.start()

    # metodo privado
    def __inicializarReconhecedorDeVeiculo(self):
        self.__reconhecedorDeVeiculo.start()

    # metodo publico
    def start(self):
        # print('NodePLVR -> __init__() -> argsPLVR.aaa:', argsPLVR.aaa)
        if self.__argsPLVR.comunicadorSemFio_start:
            print('    NodePLVR -> start(): starting comunicador sem fio...')
            self.__inicializarComunicadorSemFio()
            print('    NodePLVR -> start(): comunicador sem fio started!')

        if self.__argsPLVR.reconhecedorDeVeiculo_start:
            print('    NodePLVR -> start(): starting reconhecedor de veiculo...')
            self.__inicializarReconhecedorDeVeiculo()
            print('    NodePLVR -> start(): reconhecedor de veiculo started!')

    # metodo publico
    def stop(self):
        pass

    # metodo publico
    def restart(self):
        self.stop()
        self.start()
