# Cliente
from argumentosPLVR import ArgumentosPLVR

import socket

class ClientePLVR_UDP:
    def __init__(self, argsPLVR=ArgumentosPLVR()):
        self.argsPLVR = argsPLVR
        self.IP_Servidor = '192.168.0.255'
        self.IP_Cliente = ''
        self.Porta_Servidor = 5000
        self.Porta_Cliente = 5001
        self.UDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.UDP.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.UDP.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.placas = ''

    '''
    def AbrirPortaCliente (self):
        self.UDP.bind((self.IP_Cliente, self.Porta_Cliente))
    '''

    def CarregarArquivoCliente(self):
        Arquivo_Cliente = open('Placas.txt', 'r')
        listaConteudo = Arquivo_Cliente.readlines()

        #self.placas = ''
        for linha in listaConteudo:
            self.placas += linha
            #print('placas cliente: ', self.placas)


    def EnviarMsgCliente (self, msg):
        msgBytes = bytes(msg, 'utf-8')
        self.UDP.sendto(msgBytes, (self.IP_Servidor, self.Porta_Servidor))

    def LerMsgCliente (self):
        msgBytes, servidor = self.UDP.recvfrom(1024)

        msg = msgBytes.decode()
        return msg


    def ProcessarCliente(self, msg):
        if msg == 'on':
            #Arquivo_Cliente_Enviar = open('Placas.txt', 'r')
            #return Arquivo_Cliente_Enviar.readline()

            return self.placas

        else:
            Arquivo_Cliente = open('Placas.txt', 'a')
            Arquivo_Cliente.write(msg)

            #Arquivo_Cliente_Enviar = open('Placas.txt', 'r')
            #return Arquivo_Cliente_Enviar.readline()

            return self.placas
