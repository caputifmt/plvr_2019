from argumentosPLVR import ArgumentosPLVR

import socket


class ComunicadorUDP:

    def __init__(self, argsPLVR=ArgumentosPLVR()):
        self.__argsPLVR = argsPLVR
        self.__IP_Servidor = self.__argsPLVR.comunicadorSemFio_enderecoIP  # '192.168.0.255'
        self.__portaDeEscuta = self.__argsPLVR.comunicadorSemFio_portaDeEscuta
        self.__portaParaResponder = self.__argsPLVR.comunicadorSemFio_portaPadraoParaEnviar
        self.__socketClienteUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__socketClienteUDP.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.__socketServidorUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__socketServidorUDP.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        # self.socketUDP.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.__comunicadorAtivo = True


    # servidor
    def abrirPortaDeEscutaDoServidor(self):
        self.__socketServidorUDP.bind((self.__IP_Servidor, self.__portaDeEscuta))

    # servidor
    def fecharPortaDeEscutaDoServidor(self):
        self.__socketServidorUDP.close()

    # servidor
    def lerMsgDoCliente(self):
        msgBytes, cliente = self.__socketServidorUDP.recvfrom(1024)  # recebe a mensagem
        msg = msgBytes.decode()
        return msg, cliente


    # servidor
    def responderCliente(self, msgResposta, cliente):
        ipCliente, portaCliente = cliente
        msgRespostaBytes = bytes(msgResposta, 'utf-8')
        self.__socketServidorUDP.sendto(msgRespostaBytes, (ipCliente, self.__argsPLVR.comunicadorSemFio_portaPadraoParaEnviar))


    # cliente
    def enviarMsgParaServidor(self, msg):
        msgBytes = bytes(msg, 'utf-8')
        # self.__socketUDP.sendto(msgBytes, (self.__argsPLVR.comunicadorSemFio_enderecoIP, self.__argsPLVR.comunicadorSemFio_portaDeEscuta))
        self.__socketClienteUDP.sendto(msgBytes, (self.__argsPLVR.comunicadorSemFio_enderecoIP, self.__argsPLVR.comunicadorSemFio_portaDeEscuta))
