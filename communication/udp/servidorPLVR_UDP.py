# servidor

from argumentosPLVR import ArgumentosPLVR

import socket


class ServidorPLVR_UDP:
    def __init__(self, argsPLVR=ArgumentosPLVR()):
        self.argsPLVR = argsPLVR
        self.IP_Servidor = self.argsPLVR.comunicadorSemFio_enderecoIP
        # self.IP_Cliente = ''
        self.Porta_Servidor = self.argsPLVR.comunicadorSemFio_portaServidor
        self.Porta_Cliente = 5001
        self.socketUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.placas = ''

    def inicializarServidor(self):
        self.socketUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socketUDP.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        # self.socketUDP.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    def abrirPortaServidor(self):
        self.socketUDP.bind((self.IP_Servidor, self.Porta_Servidor))

    def fecharPortaServidor(self):
        self.socketUDP.close()

    def enviarResposta(self, msg):
        msgBytes = bytes(msg, 'utf-8')
        self.socketUDP.sendto(msgBytes, (self.IP_Cliente, self.Porta_Cliente))

    def lerMsgDoCliente(self):
        msgBytes, cliente = self.socketUDP.recvfrom(1024)

        msg = msgBytes.decode()
        return msg

    def processarMsgRecebida(self, msg):
        if msg == 'online?':
            return 'on'
        else:
            return 'error'

        '''
        else:
            Arquivo_servidor_Ler = open('Teste_Placas.txt', 'a')
            Arquivo_servidor_Ler.write(msg)

            #Arquivo_servidor_Enviar = open('Teste_Placas.txt', 'r')
            #return Arquivo_servidor_Enviar.readline()

            return self.placas
        '''

    def CarregarArquivoServidor(self):
        Arquivo_Servidor = open('Teste_Placas.txt', 'r')
        listaConteudo = Arquivo_Servidor.readlines()

        # self.placas = ''
        for linha in listaConteudo:
            self.placas += linha
            # print('placas cliente: ', self.placas)