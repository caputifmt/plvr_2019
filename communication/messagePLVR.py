

class MessagePLVR:

    ONLINE_PERGUNTA = 'online?'
    ONLINE_RESPOSTA = 'on'
    ERRO = 'error'

    def __init__(self, msg=''):
        dadosMsg = msg.split(';')
        if len(dadosMsg) > 2:
            self.id = int(dadosMsg[0])
            self.port = int(dadosMsg[1])
            self.text = dadosMsg[2]
        else:
            self.id = 0
            self.port = 5000
            self.text = MessagePLVR.ONLINE_PERGUNTA

    def setId(self, id):
        self.id = id

    def getId(self):
        return self.id

    def setPort(self, port):
        self.port = port

    def getPort(self):
        return self.port

    def setText(self, text):
        self.text = text

    def getText(self):
        return self.text

    def getMessage(self):
        self.msg = str(self.id)
        self.msg += ';' + str(self.port)
        self.msg += ';' + str(self.text)
        return self.msg

    def __str__(self):
        return self.getMessage()
