from argumentosPLVR import ArgumentosPLVR
from communication.udp.comunicadorUDP import ComunicadorUDP
from communication.messagePLVR import MessagePLVR

import time
import threading


class ComunicadorPLVR:

    def __init__(self, argsPLVR=ArgumentosPLVR()):
        self.__argsPLVR = argsPLVR
        self.__id = self.__argsPLVR.comunicadorSemFio_nodeID
        self.__comunicadorUDP = ComunicadorUDP(self.__argsPLVR)
        self.__threadCliente = threading.Thread()
        self.__threadServidor = threading.Thread()
        self.__comunicadorAtivo = True

    # servidor
    # metodo privado
    def __processarMsgRecebidaDoCliente(self, msgPLVR=MessagePLVR()):
        resposta = ''
        if msgPLVR.getText() == MessagePLVR.ONLINE_PERGUNTA:
            resposta = MessagePLVR.ONLINE_RESPOSTA
        else:
            resposta = MessagePLVR.ERRO
        return resposta


    # metodo privado
    def __loopCliente(self):
        while True:
            time.sleep(self.__argsPLVR.comunicadorSemFio_timeToSleep)

            if not self.__comunicadorAtivo:
                break

            # monta a mensagem
            msgPLVR = MessagePLVR()
            msgPLVR.setId(str(self.__argsPLVR.comunicadorSemFio_nodeID))
            msgPLVR.setPort(self.__argsPLVR.comunicadorSemFio_portaDeEscuta)
            msgPLVR.setText(MessagePLVR.ONLINE_PERGUNTA)

            self.__comunicadorUDP.enviarMsgParaServidor(msgPLVR.getMessage())
            print('            ComunicadorPLVR -> Cliente ' + str(self.__argsPLVR.comunicadorSemFio_nodeID) + ': enviando msg na porta', str(self.__argsPLVR.comunicadorSemFio_portaPadraoParaEnviar) + ': \"' + msgPLVR.getMessage() + '\"')


    # metodo privado
    def __loopServidor(self):
        print('            ComunicadorPLVR -> __loopServidor(): abrindo porta', self.__argsPLVR.comunicadorSemFio_portaDeEscuta)
        self.__comunicadorUDP = ComunicadorUDP(self.__argsPLVR)
        self.__comunicadorUDP.abrirPortaDeEscutaDoServidor()

        print('            ComunicadorPLVR -> __loopServidor(): aguardando msg do cliente...')
        while True:
            if not self.__comunicadorAtivo:
                self.__comunicadorUDP.fecharPortaDeEscutaDoServidor()
                break

            msgString, cliente = self.__comunicadorUDP.lerMsgDoCliente()
            msgPLVR = MessagePLVR(msgString)

            if msgPLVR.getId() != self.__argsPLVR.comunicadorSemFio_nodeID:  # se receber msg de outro computador
                print('            ComunicadorPLVR -> __loopServidor(): msg recebida do cliente ' + str(cliente) + ':', msgString, end='')

                respostaParaCliente = self.__processarMsgRecebidaDoCliente(msgPLVR)
                # print('Servidor Processou: ', MSG_Servidor)

                self.__comunicadorUDP.responderCliente(respostaParaCliente, cliente)
                print('\nServidor', self.__argsPLVR.comunicadorSemFio_nodeID, ': resposta enviada para cliente: ', '\"' + respostaParaCliente + '\"')
            else:
                pass
                # print('            ComunicadorPLVR -> __loopServidor(): msg recebida do cliente ' + str(cliente) + ':', msgString, end='')
                # print(' (ignorando propria msg)')


    # metodo privado
    def __inicializarClienteUDP(self):
        self.threadCliente = threading.Thread(target=self.__loopCliente)
        self.threadCliente.start()
        # self.threadServidor.join()

    # metodo privado
    def __inicializarServidorUDP(self):
        self.threadServidor = threading.Thread(target=self.__loopServidor)
        self.threadServidor.start()
        # self.threadServidor.join()

    # metodo publico
    def start(self):
        print('        ComunicadorPLVR -> start(): starting servidor UDP...')
        self.__inicializarServidorUDP()
        print('        ComunicadorPLVR -> start(): servidor UDP started!')

        print('        ComunicadorPLVR -> start(): starting cliente UDP...')
        self.__inicializarClienteUDP()
        print('        ComunicadorPLVR -> start(): cliente UDP started!')

    # metodo publico
    def stop(self):
        pass
